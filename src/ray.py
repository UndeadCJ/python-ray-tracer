from random import SystemRandom
import sys
from src.vec3 import Vec3 as vec3

class Ray(object):

    def __init__(self, a=None, b=None):
        
        self.A = a if a else vec3([0, 0, 0])
        self.B = b if b else vec3([0, 0, 0])

    @property
    def origin(self):
        return self.A

    @property
    def direction(self):
        return self.B

    def point_at_parameter(self, t):
        return self.A + t * self.B

# teste = SystemRandom.getrandbits(1, 48)
# print(teste)
# print(sys.getsizeof(teste))