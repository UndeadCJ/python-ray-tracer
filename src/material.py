import os

from src.ray import Ray as ray
from src.vec3 import Vec3 as vec3


class Material(object):

    def __init__(self, a):

        self.albedo = a

    def reflect(self, v, n):
        return v - 2 * vec3.dot(v, n) * n

    def random_in_unit_sphere(self):

        p = 2.0 * vec3([int.from_bytes(os.urandom(48), byteorder="big") / ((1 << 384) - 1),
                            int.from_bytes(os.urandom(48), byteorder="big") / ((1 << 384) - 1),
                            int.from_bytes(os.urandom(48), byteorder="big") / ((1 << 384) - 1)
                            ]) - vec3([1, 1, 1])

        while p.squared_length(p.vec3) >= 1.0:
            p = 2.0 * vec3([int.from_bytes(os.urandom(48), byteorder="big") / ((1 << 384) - 1),
                            int.from_bytes(os.urandom(48), byteorder="big") / ((1 << 384) - 1),
                            int.from_bytes(os.urandom(48), byteorder="big") / ((1 << 384) - 1)
                            ]) - vec3([1, 1, 1])
        
        return p


class Lambertian(Material):

    def scatter(self, r_in, rec):
        target = rec.p + rec.normal + self.random_in_unit_sphere()
        scattered = ray(rec.p, target - rec.p)
        attenuation = self.albedo
        return scattered, attenuation

class Metal(Material):
    
    def scatter(self, r_in, rec):
        reflected = self.reflect(vec3.unit_vector(r_in.direction), rec.normal)
        scattered = ray(rec.p, reflected)
        attenuation = self.albedo
        
        value = vec3.dot(scattered.direction, rec.normal)

        return scattered, attenuation
