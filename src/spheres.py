import math
from src.vec3 import Vec3 as vec3

class Sphere(object):
    def __init__(self, cen, r, m):
        
        self.center = cen
        self.radius = r
        self.material = m


    def hit(self, r, t_min, t_max, rec):

        oc = vec3(r.origin - self.center)

        a = vec3.dot(r.direction, r.direction)
        b = vec3.dot(oc, r.direction)
        c = vec3.dot(oc, oc) - self.radius * self.radius
        discriminant = b * b - a * c

        if discriminant > 0:
            temp = ( -b - math.sqrt(b * b - a * c)) / a
            if temp < t_max and temp > t_min:
                rec.t = temp
                rec.p = r.point_at_parameter(rec.t)
                rec.normal = (rec.p - self.center) / self.radius
                rec.material = self.material
                return True
        
            temp = (-b + math.sqrt(b * b -a * c)) / a
            if temp < t_max and temp > t_min:
                rec.t = temp
                rec.p = r.point_at_parameter(rec.t)
                rec.normal = (rec.p - self.center) / self.radius
                rec.material = self.material
                return True
        
        return False


class hit_record(object):
    def __init__(self, t, p, normal, material):

        self.t = t
        self.p = p
        self.normal = normal
        self.material = material


class hitable_list(object):
    def __init__(self, l, n):

        self.list = l
        self.list_size = n

    
    def hit(self, r, t_min, t_max=float, rec=hit_record):
        
        temp_rec = hit_record
        hit_anything = False
        closest_so_far = t_max

        for i in range(self.list_size):
            if self.list[i].hit(r, t_min, closest_so_far, temp_rec):
                hit_anything = True
                closest_so_far = temp_rec.t
                rec = temp_rec

        return hit_anything