import math


class Vec3(object):
    def __init__(self, vec3=None):

        if type(vec3) is list and len(vec3) == 3:
            self.vec3 = vec3
        elif vec3 is None:
            self.vec3 = [0, 0, 0]
        elif type(vec3) is Vec3:
            self.vec3 = [vec3.x, vec3.y, vec3.z]
        else:
            raise ValueError("Classe Vec3 suporta apenas listas tridimensionais ou None")

    # Verifica adições feitas pela esquerda
    def __add__(self, v2):

        if type(v2) is float or type(v2) is int:
            return Vec3([self.x + v2, self.y + v2, self.z + v2])

        elif isinstance(v2, Vec3):
            return Vec3([self.x + v2.x, self.y + v2.y, self.z + v2.z])


    def __radd__(self, v2):
        return self.__add__(v2)

    def __mul__(self, v2):
        
        if type(v2) is float or type(v2) is int:
            return Vec3([self.x * v2, self.y * v2, self.z * v2])

        elif isinstance(v2, Vec3):
            return Vec3([self.x * v2.x, self.y * v2.y, self.z * v2.z])

    def __rmul__(self, v2):
        return self.__mul__(v2)

    def __sub__(self, v2):
        if type(v2) is float or type(v2) is int:
            return Vec3([self.x - v2, self.y - v2, self.z - v2])

        elif isinstance(v2, Vec3):
            return Vec3([self.x - v2.x, self.y - v2.y, self.z - v2.z])

    def __rsub__(self, v2):
        return self.__sub__(v2)

    def __truediv__(self, v2):
        if type(v2) is float or type(v2) is int:
            return Vec3([self.x / v2, self.y / v2, self.z / v2])

        elif isinstance(v2, Vec3):
            return Vec3([self.x / v2.x, self.y / v2.y, self.z / v2.z])

    def __rtruediv__(self, v2):
        return self.__truediv__(v2)

    def __idiv__(self, v2):
        if type(v2) is float or type(v2) is int:
            k = 1.0 / v2
            return Vec3([self.x * k, self.y * k, self.z * k ])

        elif isinstance(v2, Vec3):
            return Vec3([self.x / v2.x, self.y / v2.y, self.z / v2.z])

    def _iadd(self, v2):
        if type(v2) is float or type(v2) is int:
            return Vec3([self.x + v2, self.y + v2, self.z + v2])

        elif isinstance(v2, Vec3):
            return Vec3([self.x + v2.x, self.y + v2.y, self.z + v2.z])


    @property
    def x(self):
        return self.vec3[0]
        
    @property
    def y(self):
        return self.vec3[1]
    
    @property
    def z(self):
        return self.vec3[2]
    
    @classmethod
    def dot(cls, v1, v2):
        if isinstance(v1, Vec3) and isinstance(v2, Vec3):
            return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z

    @classmethod
    def squared_length(cls, vec3):
        return vec3[0] * vec3[0] + vec3[1] * vec3[1] + vec3[2] * vec3[2]

    @classmethod
    def length(cls, vec3):
        return math.sqrt(Vec3.squared_length(vec3))

    @classmethod
    def unit_vector(cls, v):
        return v / v.length(v.vec3)