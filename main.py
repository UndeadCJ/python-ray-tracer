import numpy as np
from PIL import Image
import math
from multiprocessing import cpu_count
from concurrent.futures import ProcessPoolExecutor
import time
import sys
import os

from src.vec3 import Vec3 as vec3
from src.ray import Ray as ray
from src.spheres import Sphere, hit_record, hitable_list
from src.camera import Camera as cam
from src.material import Metal, Lambertian, Material


# Variáveis
width = 600
height = 300
pixel_depth = 3
sample_number = 5
number_of_processes = cpu_count() if cpu_count else 1
image_array = np.zeros((height, width, pixel_depth)).astype(np.uint8)


def color(r, world, depth):

    rec = hit_record

    if world.hit(r, 0.001, sys.maxsize, rec):

        scattered, attenuation = rec.material.scatter(r, rec)

        if depth < 50:
            return attenuation * color(scattered, world, depth + 1)

        else:
            return vec3([0, 0, 0])
    
    else:
        unit_direction = vec3.unit_vector(r.direction)
        t = 0.5 * (unit_direction.y + 1.0)
        return (1.0 - t) * vec3([1.0, 1.0, 1.0]) + t * vec3([0.5, 0.7, 1.0])


def render_node(width, height, sample_number, image_slice):

    lower_left_corner = vec3([-2.0, -1.0, -1.0])
    horizontal = vec3([4.0, 0.0, 0.0])
    vertical = vec3([0.0, 2.0, 0.0])
    origin = vec3([0.0, 0.0, 0.0])

    hit_list = [
        Sphere(vec3([0, 0, -1]), 0.5, Lambertian(vec3([0.8, 0.3, 0.3]))),
        Sphere(vec3([0, -100.5, -1]), 100, Lambertian(vec3([0.8, 0.8, 0.0]))),
        Sphere(vec3([1, 0, -1]), 0.5, Metal(vec3([0.8, 0.6, 0.2]))),
        Sphere(vec3([-1, 0, -1]), 0.5, Metal(vec3([0.8, 0.8, 0.8])))
    ]

    world = hitable_list(hit_list, 4)
    camera = cam(origin, horizontal, vertical, lower_left_corner)
    
    for j in range(height)[image_slice]:
        for i in range(width):
            col = vec3([0,0,0])
            for s in range(sample_number): 

                u = (i + int.from_bytes(os.urandom(48), byteorder="big") / ((1 << 384) - 1)) / width
                v = (j + int.from_bytes(os.urandom(48), byteorder="big") / ((1 << 384) - 1)) / height
                r = camera.get_ray(u, v)
                p = r.point_at_parameter(2.0)
                col += color(r, world, 0)
            
            col /= sample_number

            col = vec3([math.sqrt(col.x), math.sqrt(col.y), math.sqrt(col.z)])
            ir = 255.99 * col.x
            ig = 255.99 * col.y
            ib = 255.99 * col.z

            image_array[j][i] = [ir,ig,ib]

    return image_array

    
def prepare_node(height, n):

    slice_size = round(height / n)
    dict_slices = []

    old_slice_size = 0
    new_slice_size = slice_size

    for i in range(n):

        if i == 0:
            dict_slices.append(slice(0, new_slice_size))
        
        else:
            dict_slices.append(slice(old_slice_size, new_slice_size))
        
        old_slice_size = new_slice_size
        new_slice_size += slice_size

    return dict_slices


def Render(width, height, pixel_depth, sample_number, nodes):

    global image_array

    start = time.perf_counter()

    print("Iniciando...")

    number_of_render_nodes = nodes

    dict_slices = prepare_node(height, number_of_render_nodes)

    with ProcessPoolExecutor() as executor:

        processes = []
        
        for node in range(number_of_render_nodes):
            f1 = executor.submit(render_node, width, height, sample_number, dict_slices[node])
            print(f"Núcleo {node} iniciado")
            processes.append(f1)
        
        print(f"Inicializado, núcleos em execução: {number_of_render_nodes}")

        arrays_list = []

        for process in processes:
            arrays_list.append(np.array(process.result()))

        
        for i in range(len(arrays_list)):
            image_array[dict_slices[i]] = arrays_list[i][dict_slices[i]]
            

    output = Image.fromarray(image_array).transpose(Image.FLIP_TOP_BOTTOM)
    output.save("output.bmp")

    finish = time.perf_counter()

    print(f"Renderizado em {round(finish - start, 2)} segundos")


if __name__ == '__main__':

    Render(width, height, pixel_depth, sample_number, number_of_processes)
